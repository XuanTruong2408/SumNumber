B1ước 1: clone respository về máy (phải có sẵn môi trường python)

├── input
│   ├── expression.txt
├── src
│   ├── NumberPackage
│   │   ├── __pycahe
│   │   ├── number.py
│   ├── main.py
├── .notebook
└── README.md

Bước 2: nhập dữ liệu các số vào file expression.txt theo định dạng "number1, number2"  trên mỗi dòng
    Ví dụ: 
    1278738, 172878
    273871, 4182787
Bước 3: chạy file main.py trên terminal. Kết quả sẽ hiển thị trên màn hình console