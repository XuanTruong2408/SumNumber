class MyBigNumber:
    number1 = ''
    number2 = ''
    def __init__(self, number1, number2):
        # đưa số có số lượng kí tự dài hơn lên trước
        if len(number1) < len(number2):
            self.number1, self.number2 = number2, number1
        else:
            self.number1, self.number2 = number1, number2
            
    # xuất ra phép toán
    def checkExpression(self):
        print("phép tính: {N1} + {N2}".format(N1 = self.number1, N2 = self.number2))
        
    # thực thi cộng 2 số
    def sumNumber(self):
        number1 =  self.number1
        number2 =  self.number2
        
        R = 0;
        result = ''
        for i in range(len(number1)):
            in1 = len(number1) - i - 1
            in2 = len(number2) - i - 1
            digit1 = 0 if in1 < 0 else int(number1[in1])
            digit2 = 0 if in2 < 0 else int(number2[in2])
            rb = R
            sum = int(digit1) + int(digit2)
            childResult = (sum + R) % 10
            R = (sum + R) // 10
            result = str(childResult) + result
            if rb == 0:
                if R == 0:
                    print("""lấy {d1} cộng với {d2} được {sum}, ghi {cr} vào kết quả. 
                          Kết quả hiện tại là {rs}""".format(d1=digit1,
                                                             d2=digit2,
                                                             sum=sum,
                                                             cr=childResult,
                                                             rs=result))
                else:
                    print("""lấy {d1} cộng với {d2} được {sum}, ghi {cr} vào kết quả và nhớ 1. 
                          Kết quả hiện tại là {rs}""".format(d1=digit1,
                                                             d2=digit2,
                                                             sum=sum,
                                                             cr=childResult,
                                                             rs=result))
            else:
                if R == 0:
                    print("""lấy {d1} cộng với {d2} được {sum}, cộng tiếp cho 1 ta được {sumr}, ghi {cr} vào kết quả. 
                          Kết quả hiện tại là {rs}""".format(d1=digit1,
                                                             d2=digit2,
                                                             sum=sum,
                                                             sumr=sum + 1,
                                                             cr=childResult,
                                                             rs=result))
                else:
                    print("""lấy {d1} cộng với {d2} được {sum}, cộng tiếp cho 1 ta được {sumr}, ghi {cr} vào kết quả và nhớ 1. 
                          Kết quả hiện tại là {rs}""".format(d1=digit1,
                                                             d2=digit2,
                                                             sum=sum,
                                                             sumr=sum + 1,
                                                             cr=childResult,
                                                             rs=result))
            if in2 < 0:
                break
        if R > 0:
            result = '1' +result
            print("Hạ nhớ 1 xuống ta được kết quả {rs}".format(rs=result))
        if (len(number1) - len(number2) > 0):
            result = number1[:(len(number1) - len(number2) -1)] + result
            print("thêm phần {big} xuống kết quả ta được {rs}".format(big=number1[:(len(number1) - len(number2) - 1)], 
                                                                    rs=result))

            
        
        # # lấy số lượng kí tự chênh lệch
        # lenDistance = len(number1) - len(number2)
        # if lenDistance > 0:
        #     number2 = '0' + number2 
            
        # # bắt đầu thực hiện
        # R = 0
        # result = ''
        
        
        # while (number2 != ''):
        #     digit1, digit2 = number1[-1], number2[-1]  
        #     rb = R
        #     sum = int(digit1) + int(digit2)
        #     childResult = (sum + R) % 10
        #     R = (sum + R) // 10
        #     result = str(childResult) + result
        #     if rb == 0:
        #         if R == 0:
        #             print("""lấy {d1} cộng với {d2} được {sum}, ghi {cr} vào kết quả. 
        #                   Kết quả hiện tại là {rs}""".format(d1=digit1,
        #                                                      d2=digit2,
        #                                                      sum=sum,
        #                                                      cr=childResult,
        #                                                      rs=result))
        #         else:
        #             print("""lấy {d1} cộng với {d2} được {sum}, ghi {cr} vào kết quả và nhớ 1. 
        #                   Kết quả hiện tại là {rs}""".format(d1=digit1,
        #                                                      d2=digit2,
        #                                                      sum=sum,
        #                                                      cr=childResult,
        #                                                      rs=result))
        #     else:
        #         if R == 0:
        #             print("""lấy {d1} cộng với {d2} được {sum}, cộng tiếp cho 1 ta được {sumr}, ghi {cr} vào kết quả. 
        #                   Kết quả hiện tại là {rs}""".format(d1=digit1,
        #                                                      d2=digit2,
        #                                                      sum=sum,
        #                                                      sumr=sum + 1,
        #                                                      cr=childResult,
        #                                                      rs=result))
        #         else:
        #             print("""lấy {d1} cộng với {d2} được {sum}, cộng tiếp cho 1 ta được {sumr}, ghi {cr} vào kết quả và nhớ 1. 
        #                   Kết quả hiện tại là {rs}""".format(d1=digit1,
        #                                                      d2=digit2,
        #                                                      sum=sum,
        #                                                      sumr=sum + 1,
        #                                                      cr=childResult,
        #                                                      rs=result))
        #     number1, number2 = number1[:-1], number2[:-1]   
        # if lenDistance == 0 and R == 1:
        #     result = '1' +result
        #     print("Hạ nhớ 1 xuống ta được kết quả {rs}".format(rs=result))
        # # kiểm tra phần thừa khi số nhỏ hơn đã hết
        # elif number1 != '':
        #     result = number1 + result
        #     print("thêm phần {big} xuống kết quả ta được {rs}".format(big=number1, 
        #                                                               rs=result))
        return result        
