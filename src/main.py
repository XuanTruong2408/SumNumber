import logging
import NumberPackage.number as number
import time

logging.basicConfig(level=logging.DEBUG)

def main():
    try:
        ## read data 
        start = time.time()
        with open('./input/expression.txt') as f:
            df = f.readlines()
            for lines in df:
                n1, n2 = lines.split(', ')
                n2 = n2.strip()
                object = number.MyBigNumber(n1,n2)
                object.checkExpression()
                print("kết quả: {rs} \n".format(rs = object.sumNumber()), '-'*100)
            f.close()
        end = time.time()
        #tính thời gian chạy của thuật toán Python
        elapsed_time = end - start
        print ("elapsed_time:{0}".format(elapsed_time) + "[sec]")


    except:
        logging.warning("something went wrong...")

if __name__ == "__main__":
    main()
